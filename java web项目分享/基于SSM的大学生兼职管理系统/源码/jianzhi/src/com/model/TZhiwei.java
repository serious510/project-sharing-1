package com.model;

/**
 * TZhiwei entity. @author MyEclipse Persistence Tools
 */

public class TZhiwei implements java.io.Serializable
{

	// Fields

	private Integer id;
	private Integer qiyeId;
	private Integer leibieId;
	private String mingcheng;
	private String xueli;
	private String daiyu;
	private String didian;
	private String jingyan;
	private String beizhu;
	private String fabushi;
	private String del;
	
	private TQiye qiye;

	// Constructors

	/** default constructor */
	public TZhiwei()
	{
	}

	/** full constructor */
	public TZhiwei(Integer qiyeId, Integer leibieId, String mingcheng,
			String xueli, String daiyu, String didian, String jingyan,
			String beizhu, String fabushi, String del)
	{
		this.qiyeId = qiyeId;
		this.leibieId = leibieId;
		this.mingcheng = mingcheng;
		this.xueli = xueli;
		this.daiyu = daiyu;
		this.didian = didian;
		this.jingyan = jingyan;
		this.beizhu = beizhu;
		this.fabushi = fabushi;
		this.del = del;
	}

	// Property accessors

	public Integer getId()
	{
		return this.id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Integer getQiyeId()
	{
		return this.qiyeId;
	}

	public TQiye getQiye()
	{
		return qiye;
	}

	public void setQiye(TQiye qiye)
	{
		this.qiye = qiye;
	}

	public void setQiyeId(Integer qiyeId)
	{
		this.qiyeId = qiyeId;
	}

	public Integer getLeibieId()
	{
		return this.leibieId;
	}

	public void setLeibieId(Integer leibieId)
	{
		this.leibieId = leibieId;
	}

	public String getMingcheng()
	{
		return this.mingcheng;
	}

	public void setMingcheng(String mingcheng)
	{
		this.mingcheng = mingcheng;
	}

	public String getXueli()
	{
		return this.xueli;
	}

	public void setXueli(String xueli)
	{
		this.xueli = xueli;
	}

	public String getDaiyu()
	{
		return this.daiyu;
	}

	public void setDaiyu(String daiyu)
	{
		this.daiyu = daiyu;
	}

	public String getDidian()
	{
		return this.didian;
	}

	public void setDidian(String didian)
	{
		this.didian = didian;
	}

	public String getJingyan()
	{
		return this.jingyan;
	}

	public void setJingyan(String jingyan)
	{
		this.jingyan = jingyan;
	}

	public String getBeizhu()
	{
		return this.beizhu;
	}

	public void setBeizhu(String beizhu)
	{
		this.beizhu = beizhu;
	}

	public String getFabushi()
	{
		return this.fabushi;
	}

	public void setFabushi(String fabushi)
	{
		this.fabushi = fabushi;
	}

	public String getDel()
	{
		return this.del;
	}

	public void setDel(String del)
	{
		this.del = del;
	}

}