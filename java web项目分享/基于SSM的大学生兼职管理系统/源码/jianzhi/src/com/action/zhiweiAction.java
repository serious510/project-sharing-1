package com.action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.dao.TJianliDAO;
import com.dao.TQiyeDAO;
import com.dao.TZhiweiDAO;
import com.model.TQiye;
import com.model.TZhiwei;
import com.opensymphony.xwork2.ActionSupport;

public class zhiweiAction extends ActionSupport
{
	private Integer id;
	private Integer qiyeId;
	private Integer leibieId;
	private String mingcheng;
	private String xueli;

	private String daiyu;
	private String didian;
	private String jingyan;
	private String beizhu;
	
	private TZhiweiDAO zhiweiDAO;
	private TQiyeDAO qiyeDAO;
	private TJianliDAO jianliDAO;
	
	
	public String zhiwei_add_qiye()
	{
		HttpServletRequest request=ServletActionContext.getRequest();
		HttpSession session=request.getSession();
		TQiye qiye=(TQiye)session.getAttribute("qiye");
		
		TZhiwei zhiwei=new TZhiwei();
		
		zhiwei.setQiyeId(qiye.getId());
		zhiwei.setLeibieId(leibieId);
		zhiwei.setMingcheng(mingcheng);
		zhiwei.setXueli(xueli);
		zhiwei.setDaiyu(daiyu);
		
		zhiwei.setDidian(didian);
		zhiwei.setJingyan(jingyan);
		zhiwei.setBeizhu(beizhu);
		zhiwei.setFabushi(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()));
		
		zhiwei.setDel("no");
		
		zhiweiDAO.save(zhiwei);
		
		request.setAttribute("msg", "职位添加完毕");
		return "msg";
		
		
	}
	
	public String zhiwei_mana_qiye()
	{
		HttpServletRequest request=ServletActionContext.getRequest();
		HttpSession session=request.getSession();
		TQiye qiye=(TQiye)session.getAttribute("qiye");
		
		String sql="from TZhiwei where del='no' and qiyeId="+qiye.getId();
		List zhiweiList=zhiweiDAO.getHibernateTemplate().find(sql);
		
		request.setAttribute("zhiweiList", zhiweiList);
		return ActionSupport.SUCCESS;
	}
	
	
	public String zhiwei_del()
	{
		HttpServletRequest request=ServletActionContext.getRequest();
		
		TZhiwei zhiwei=zhiweiDAO.findById(id);
		zhiwei.setDel("yes");
		zhiweiDAO.attachDirty(zhiwei);
		
		request.setAttribute("msg", "职位删除完毕");
		return "msg";
	}
	
	
	public String zhiwei_jianli()
	{
		HttpServletRequest request=ServletActionContext.getRequest();
		
		String sql="from TJianli where zhiweiId="+Integer.parseInt(request.getParameter("zhiweiId"));
		List jianliList=jianliDAO.getHibernateTemplate().find(sql);
		
		request.setAttribute("jianliList", jianliList);
        return ActionSupport.SUCCESS;
	}
	
	
	public String zhiweiByLeibie()
	{
		HttpServletRequest request=ServletActionContext.getRequest();
		
		String sql="from TZhiwei where del='no' and leibieId="+leibieId;
		List zhiweiList=zhiweiDAO.getHibernateTemplate().find(sql);
		
		request.setAttribute("zhiweiList", zhiweiList);
		return ActionSupport.SUCCESS;
	}
	
	public String zhiweiDetailQian()
	{
		HttpServletRequest request=ServletActionContext.getRequest();
		
		TZhiwei zhiwei=zhiweiDAO.findById(id);
		zhiwei.setQiye(qiyeDAO.findById(zhiwei.getQiyeId()));
		request.setAttribute("zhiwei", zhiwei);
		
		return ActionSupport.SUCCESS;
	}
	
	
	public String zhiweiRes()
	{
		String sql="from TZhiwei where del='no' and mingcheng like '%"+mingcheng.trim()+"%'";
		List zhiweiList=zhiweiDAO.getHibernateTemplate().find(sql);
		
		HttpServletRequest request=ServletActionContext.getRequest();
		request.setAttribute("zhiweiList", zhiweiList);
		return ActionSupport.SUCCESS;
	}

	public String getBeizhu()
	{
		return beizhu;
	}

	public void setBeizhu(String beizhu)
	{
		this.beizhu = beizhu;
	}

	public TJianliDAO getJianliDAO()
	{
		return jianliDAO;
	}

	public void setJianliDAO(TJianliDAO jianliDAO)
	{
		this.jianliDAO = jianliDAO;
	}

	public String getDaiyu()
	{
		return daiyu;
	}

	public Integer getLeibieId()
	{
		return leibieId;
	}

	public void setLeibieId(Integer leibieId)
	{
		this.leibieId = leibieId;
	}

	public void setDaiyu(String daiyu)
	{
		this.daiyu = daiyu;
	}

	public String getDidian()
	{
		return didian;
	}

	public void setDidian(String didian)
	{
		this.didian = didian;
	}

	public TQiyeDAO getQiyeDAO()
	{
		return qiyeDAO;
	}

	public void setQiyeDAO(TQiyeDAO qiyeDAO)
	{
		this.qiyeDAO = qiyeDAO;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getJingyan()
	{
		return jingyan;
	}

	public void setJingyan(String jingyan)
	{
		this.jingyan = jingyan;
	}

	public String getMingcheng()
	{
		return mingcheng;
	}

	public void setMingcheng(String mingcheng)
	{
		this.mingcheng = mingcheng;
	}

	public Integer getQiyeId()
	{
		return qiyeId;
	}

	public void setQiyeId(Integer qiyeId)
	{
		this.qiyeId = qiyeId;
	}

	public String getXueli()
	{
		return xueli;
	}

	public void setXueli(String xueli)
	{
		this.xueli = xueli;
	}

	public TZhiweiDAO getZhiweiDAO()
	{
		return zhiweiDAO;
	}

	public void setZhiweiDAO(TZhiweiDAO zhiweiDAO)
	{
		this.zhiweiDAO = zhiweiDAO;
	}
	
}
